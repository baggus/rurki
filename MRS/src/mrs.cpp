#include <iostream>
using namespace std;

double *solve(double **t, int N) {

	double *u = new double[N + 1];

	for (int i = 0; i < 4; i++) {
		double t01 = t[0][1];
		t[0][i] /= t01;
	}

	for (int i = 1; i < N + 1; i++) {
		double ti0 = t[i][0];
		for (int j = 0; j < 3; j++) {
			t[i][j] = t[i][j] - t[i - 1][j + 1] * ti0;
		}
		double ti1 = t[i][1];
		for (int j = 0; j < 4; j++) {
			t[i][j] = t[i][j] / ti1;
		}
	}

	u[N] = t[N][3] / t[N][1];
	for (int i = N - 1; i >= 0; i--) {
		u[i] = -t[i][2] * u[i + 1];
	}
	return u;
}

void insertFirstRow(double **t, double a1, double a2, double a3) {

	t[0][1] = a1;
	t[0][2] = a2;
	t[0][3] = a3;
}

void insertFirstRowFromConsole(double **t) {

	cout << "first row" << endl;

	double a1, a2, a3;
	cout << "a1 = ?" << endl;
	cin >> a1;
	cout << "a2 = ?" << endl;
	cin >> a2;
	cout << "a3 = ?" << endl;
	cin >> a3;
	insertFirstRow(t, a1, a2, a3);
}

void insertMiddleRow(double **t, int rowNumber, double a0, double a1, double a2,
		double a3) {
	t[rowNumber][0] = a0;
	t[rowNumber][1] = a1;
	t[rowNumber][2] = a2;
	t[rowNumber][3] = a3;
}

void insertMiddleRowFromConsole(double **t, int N) {

	cout << "middle row" << endl;

	double a0, a1, a2, a3;
	cout << "a0 = ?" << endl;
	cin >> a0;
	cout << "a1 = ?" << endl;
	cin >> a1;
	cout << "a2 = ?" << endl;
	cin >> a2;
	cout << "a3 = ?" << endl;
	cin >> a3;
	insertMiddleRow(t, N, a0, a1, a2, a3);
}

void insertLastRow(double **t, int N, double a0, double a1, double a3) {

	t[N][0] = a0;
	t[N][1] = a1;
	t[N][3] = a3;
}

void insertLastRowFromConsole(double **t, int N) {

	cout << "last row" << endl;

	double a0, a1, a3;
	cout << "a0 = ?" << endl;
	cin >> a0;
	cout << "a1 = ?" << endl;
	cin >> a1;
	cout << "a3 = ?" << endl;
	cin >> a3;
	insertLastRow(t, N, a0, a1, a3);
}

int main() {

	int N = 2000;
	double h = (double) 1 / N;
	double **t = new double*[N + 1];
	for (int i = 0; i < N + 1; i++) {
		t[i] = new double[4];
	}

	insertFirstRow(t, 1, 0, 0);

	int i;
	for(i = 1; i < N/2 ;i++) {
		insertMiddleRow(t, i, 1,-2, 1, 0);
	}
	insertMiddleRow(t, i, 1, -3, 2, 0);

	for(++i;i < N; i++) {
		insertMiddleRow(t, i, 2, -4, 2, 0);
	}


	insertLastRow(t,N,-1, h+1, h);

	for (int i = 0; i < N + 1; i++) {
		for(int j = 0; j < 4; j++) {
			cout << t[i][j] << " ";
		}
		cout << endl;
	}


	insertFirstRow(t, 2, 4, 1);
	insertMiddleRow(t, N, 1, 3, 2, 1);
	insertLastRow(t, N, 1, -1, 0.2);

	insertFirstRowFromConsole(t);
	insertMiddleRowFromConsole(t, N);
	insertLastRowFromConsole(t, N);


	double *u = solve(t, N);


	for (int i = 0; i < N + 1; i++) {
		cout << i * h * 10 << "," << u[i] << endl;
	}

	return 0;
}
